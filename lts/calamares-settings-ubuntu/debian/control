Source: calamares-settings-ubuntu
Section: devel
Priority: optional
Maintainer: Lubuntu Developers <lubuntu-devel@lists.ubuntu.com>
Uploaders: Simon Quigley <tsimonq2@ubuntu.com>
Build-Depends: debhelper-compat (= 12), intltool, qttools5-dev-tools
Standards-Version: 4.5.0
Homepage: https://code.launchpad.net/ubuntu-calamares-settings
Vcs-Git: https://git.launchpad.net/ubuntu-calamares-settings
Vcs-Browser: https://git.launchpad.net/ubuntu-calamares-settings

Package: calamares-settings-ubuntu-common
Architecture: any
Depends: calamares (>= 3.2.14~), python3, squashfs-tools, ${misc:Depends}
Description: Common Calamares Settings
 This package contains the common Calamares settings for all flavors.
 There is also a automirror Python script to set sources based on
 geolocation.

Package: calamares-settings-ubuntulumina
Architecture: any
Depends: calamares-settings-ubuntu-common (>= ${binary:Version}),
         ${misc:Depends}
Breaks: calamares-settings-ubuntustudio,
         calamares-settings-lubuntu
Conflicts: calamares-settings-ubuntustudio,
         calamares-settings-lubuntu
Description: Ubuntu Lumina Calamares Settings and Branding
 This package contains the Calamares settings and branding for Ubuntu Lumina.
 As part of the branding the installer slideshow is contained within.
 The settings ensure a proper Ubuntu Lumina desktop is installed with the
 Lumina desktop environment.

Package: calamares-settings-lubuntu
Architecture: all
Depends: calamares-settings-ubuntu-common (>= ${binary:Version}),
         ${misc:Depends}
Breaks: calamares-settings-ubuntustudio,
         calamares-settings-ubuntulumina
Conflicts: calamares-settings-ubuntustudio,
         calamares-settings-ubuntulumina
Description: Lubuntu Calamares Settings and Branding
 This package contains the Calamares settings and branding for Lubuntu.
 As part of the branding the installer slideshow is contained within.
 The settings ensure a proper Lubuntu desktop is installed with the
 LXQt desktop environment.

Package: calamares-settings-ubuntustudio
Architecture: all
Depends: calamares-settings-ubuntu-common (>= ${binary:Version}),
         ${misc:Depends}
Breaks: calamares-settings-lubuntu
Conflicts: calamares-settings-lubuntu
Description: Ubuntu Studio Calamares Settings and Branding
 This package contains the Calamares settings and branding for Ubuntu Studio.
 As part of the branding the installer slideshow is contained within.
 The settings ensure a proper Ubuntu Studio desktop is installed with the
 KDE Plasma desktop environment.
